import json
import requests
from configparser import ConfigParser
import os
import time
import string
import re
import random

articles = ['and', 'an', 'a', 'the', '&', 'to', '<i>', '</i>', 'or', 'in', '']

#Base class of all components
class Component:
    def __init__(self):
        pass
    def update(self):
        return ""
        pass
    def getMsg(self, msg):
        pass
    def shutdown(self):
        pass
    def isCommand(self, msg, cmd):
        if cmd in msg[3].lower():
            return True
        return False
    def getSender(self, msg):
        return msg[0][1:msg[0].find("!")]

class WordOfTheDay(Component):
    def __init__(self,cfg):
        super().__init__()
        self.word = cfg.get("ComponentVars", "WordOfDay").replace(' ', '').split(",")
        self.prevword = cfg.get("ComponentVars", "PrevWord")
        self.hint = cfg.get("ComponentVars", "WordHint")
        self.cooldown = cfg.getint("ComponentVars", "WordCooldown")
        self.Finders =[]
        self.previousFind = 0.0
    def getMsg(self, msg):
        if (time.time() - (self.previousFind+self.cooldown)) > 0:
            sender = self.getSender(msg)
            if sender not in self.Finders:
                for txt in msg[3:]:
                    for word in self.word:
                        txt = txt.strip(string.whitespace)
                        txt = txt.strip(string.punctuation)
                        if len(word) == len(txt) and word in txt.lower():
                            self.Finders.append(sender)
                            self.previousFind = time.time()
                            return "@"+ sender + " SAID THE MAGIC WORD AAAAAAAAAH!"
        
        if len(msg) == 4:
            if self.isCommand(msg, ":!word"):
                print("word command")
                if not self.Finders:
                    return "Try to find the word of the day and yell when someone finds it! :D | No one has found it, and yesterday's was '" + self.prevword + "'"
                else:
                    return "Try to find the word of the day and yell when someone finds it! :D | "+ str(len(self.Finders))+" chatters have found it, and yesterday's was '" + self.prevword + "'"
            if self.isCommand(msg, ":!hint") and self.hint != ".":
                return self.hint
        return ""
    
    def shutdown(self):
        pass

class HighlightTimestamp(Component):
    def __init__(self, cfg):
        self.file = None
        files = os.listdir(cfg.get("ComponentVars", "HighlightFolder"))
        filename = time.strftime("highlights-%m-%d.txt")
        if filename not in files:
            self.file = open(filename, "x")
        else:
            self.file = open(filename, 'a+')
        pass

    def getMsg(self, msg):
        if self.isCommand(msg, ":!highlight"):
            print("highlighting!")
            sender = self.getSender(msg)
            txt = " ".join(msg[4:])
            self.file.write(time.strftime("%I:%M:%S : ")+txt +"| By "+sender+"\n")
            return "highlighting!"
        return ""

    def shutdown(self):
        self.file.close()
        pass

class Quiz(Component):
    def __init__(self, cfg):
        self.activeTrivia = False
        self.activeQuestion = False
        self.previousFind = 0.0
        self.clueIndx = -1
        self.cooldown = cfg.getint("ComponentVars", "QuizQuestionSkip")
        self.skipTime = 0
        self.scoreboard = {}
        self.clues = None
        self.currentClue = None
        self.user = cfg.get("User", "UserName")
        pass

    def getNextQuestion(self):
        isQuestion = False
        while not isQuestion and self.clueIndx < len(self.clues):
            self.clueIndx += 1
            if self.clueIndx != len(self.clues):
                self.currentClue = self.clues[self.clueIndx]
            question = self.currentClue['question'].strip()
            if '[' not in question and question != "":
                isQuestion = True
        self.skipTime = time.time()
        #print("answer: " + answer)

    def update(self):
        if self.activeTrivia and (time.time() - (self.skipTime + self.cooldown)) >= 0:
            answer = self.currentClue['answer']
            self.getNextQuestion()
            return "Times up! Answer: " +answer + ". Next one: #"+str(self.clueIndx+1) +"- " + self.currentClue['category']['title'] + ": " + self.currentClue['question']
        return ""

    def NewQuiz(self):
        self.activeTrivia = True
        self.activeQuestion = True
        self.clueIndx = -1
        self.scoreboard = {}
        par = {'count': 100}
        get = requests.get('http://jService.io/api/random', params = par)
        decoder = json.JSONDecoder()
        self.clues = decoder.decode(get.text)
        self.getNextQuestion()
    
    def getMsg(self, msg):
        sender = self.getSender(msg)
        table = str.maketrans({key: None for key in string.punctuation})
        if sender == self.user:
            if self.isCommand(msg, "!startquiz"):
                '''self.activeQuestion = True
                self.activeTrivia = True
                self.getNextQuestion()'''
                self.NewQuiz()
                return "Let's start the trivia! #"+str(self.clueIndx+1)+"- " + self.currentClue['category']['title'] + ": " + self.currentClue['question']
            if self.isCommand(msg, "!skipq"):
                self.getNextQuestion()
                return "Skipped! Next: #"+str(self.clueIndx) +"- " + self.currentClue['category']['title'] + ": " + self.currentClue['question']
            if self.isCommand(msg, "!answer"):
                answer = self.currentClue['answer'].lower()
                answer = answer.translate(table)
                return "answer: " + answer
        if self.isCommand(msg, ":!quizhelp"):
            return "commands are: !repeat, !score, !topscores"
        elif self.isCommand(msg, ":!score"):
            if sender in list(self.scoreboard.keys()):
                return sender + " has " + str(self.scoreboard[sender]) + " points!"
            else:
                return "@" + sender + ", you're not on the board yet"
        elif self.isCommand(msg, ":!topscores"):
            ret = ""
            if not self.scoreboard:
                return "No one's on the board D:"
            for candidate in list(self.scoreboard.keys()):
                ret = ret + "  " + candidate + ":" + str(self.scoreboard[candidate])
            return ret
        
        if self.activeQuestion:
            if self.isCommand(msg, ":!repeat"):
                return self.currentClue['category']['title'] + ": " + self.currentClue['question']
            count = 0
            
            #Remove articles and symbols
            answer = self.currentClue['answer'].lower().strip('\r\n')
            if '<i>' in answer:
                answer = answer[3:len(answer)-4]
            
            answer = answer.translate(table)
            
            
            splitAnswer = answer.split(" ")
            splitAnswer = [x.replace(' ', '') for x in splitAnswer if x not in articles]
            print("answer: " + str(splitAnswer))
            #answer check
            for word in msg[3:]:
                fixedword = word.lower().strip()
                fixedword = fixedword.translate(table)
                if fixedword == "" or fixedword in articles:
                    continue
                
                for wordFromSplit in splitAnswer:
                    if fixedword == wordFromSplit:
                        count += 1
                        break
                    
            #correct answer
            if count == len(splitAnswer):
                #update scoreboard
                if sender in self.scoreboard:
                    self.scoreboard[sender] += 1
                else:
                    self.scoreboard[sender] = 1
                print(self.scoreboard)
                self.getNextQuestion()
                
                if self.clueIndx == len(self.clues):
                    self.activeQuestion = False
                    self.activeTrivia = False
                    return "@" + sender + " got it! - '" + self.clues[self.clueIndx-1]['answer'] + "' And that's all folks! Trivia is over! Feel free to check your score with !score!"
                
                '''self.previousFind = time.time()
                #time check for answered question
                if (time.time() - (self.previousFind+self.cooldown)) < 0:
                    self.activeQuestion = False
                    return "@" + sender + " got it! - '" + self.clues[self.clueIndx-1]['answer'] + "'"
                else:'''
                return "@" + sender + " got it! - '" + self.clues[self.clueIndx-1]['answer'] + "'." + "Next Question, #" + str(self.clueIndx+1) + "- " + self.currentClue['category']['title'] + ": " + self.currentClue['question']
        #time check for unnactive question
        if (time.time() - (self.previousFind+self.cooldown)) > 0 and not self.activeQuestion and self.activeTrivia:
            self.activeQuestion = True
            return "#" + str(self.clueIndx+1) + "- " + self.currentClue['category']['title'] + ": " + self.currentClue['question']
            
        return ""
    def shutdown(self):
        pass

class Counter(Component):
    def __init__(self,cfg):
        super().__init__()
        self.count = 0
        self.user = cfg.get("User", "UserName")
        
    def getMsg(self, msg):
        if len(msg) == 4:
            if self.isCommand(msg, ":!dead") or self.isCommand(msg, ":!death"):
                self.count = self.count + 1
                if self.count == 1:
                    return str(self.count) + " death nooberHA"
                else:
                    return str(self.count) + " deaths nooberHA"
            if self.isCommand(msg, ":!count"):
                if self.count == 1:
                    return str(self.count) + " death so far!"
                else:
                    return str(self.count) + " deaths so far!"
                
            sender = self.getSender(msg)
            if self.isCommand(msg, ":!reset") and sender == self.user:
                self.count = 0
        return ""

class DieRoll(Component):
    def __init__(self,cfg):
        super().__init__()
        self.count = 0
        
    def getMsg(self, msg):
        if len(msg) == 4:
            if self.isCommand(msg, ":!d"):
                match = re.match(r':!d(\d{1,4})(\s*\S*)?', msg[3])
                if match:
                    sender = self.getSender(msg)
                    stop = int(match.group(1))
                    return "@" + sender + " rolled a " + str( random.randrange(1, stop) ) + "!"
        return ""

''' Single component testing
cfg = ConfigParser()
cfg.read('config.ini')
WordOfTheDay(cfg)

answer = 'Heaven and Earth'.lower().strip('\r\n')
if '<i>' in answer:
    answer = answer[3:len(answer)-4]

table = str.maketrans({key: None for key in string.punctuation})            
answer = answer.translate(table)
print("answer: " + answer)
splitAnswer = answer.split(" ")
splitAnswer = [x.replace(' ', '') for x in splitAnswer if x not in articles]
print(splitAnswer)
'''
